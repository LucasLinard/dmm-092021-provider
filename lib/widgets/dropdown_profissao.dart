import 'package:flutter/material.dart';
import 'package:profissional_ti/provider/drop_down_provider.dart';
import 'package:provider/provider.dart';

class DropdownProfissao extends StatelessWidget {
  const DropdownProfissao({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<DropdownProvider>(
      builder: (context, model, child) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: DropdownButton<String>(
              onChanged: (value) {
                model.profissao = value;
              },
              value: model.profissao,
              items: model.mesAniversario.map((e) {
                return DropdownMenuItem<String>(
                    value: e.profissao,
                    child: Text(
                      e.mes,
                      style: Theme.of(context).textTheme.bodyText2,
                    ));
              }).toList()),
        );
      },
    );
  }
}
