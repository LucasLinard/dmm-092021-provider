import 'package:flutter/material.dart';
import 'package:profissional_ti/provider/drop_down_provider.dart';
import 'package:provider/provider.dart';

class TelaResultado extends StatelessWidget {
  const TelaResultado({
    Key? key,
    required this.qualidade,
  }) : super(key: key);
  final String qualidade;

  @override
  Widget build(BuildContext context) {
    print("telaResultado");
    return Scaffold(
      appBar: AppBar(
        title: const Text("Parabéns"),
      ),
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Consumer<DropdownProvider>(
                builder: (context, model, child) => Text(
                  "No seu futuro na área de TI você será um ótimo ${model.profissao} $qualidade.",
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
