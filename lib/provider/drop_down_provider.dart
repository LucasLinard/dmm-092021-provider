import 'package:flutter/cupertino.dart';
import 'package:profissional_ti/model/mes_aniversario.dart';

class DropdownProvider with ChangeNotifier {
  final List<MesAniversario> mesAniversario = [
    MesAniversario(mes: "jan", profissao: "Analista de projetos"),
    MesAniversario(mes: "fev", profissao: "Arquiteto de TI"),
    MesAniversario(mes: "mar", profissao: "Programador"),
    MesAniversario(mes: "abr", profissao: "Business intelligence"),
    MesAniversario(mes: "mai", profissao: "Help desk"),
    MesAniversario(mes: "jun", profissao: "Administrador de banco de dados"),
    MesAniversario(mes: "jul", profissao: "Desenvolvedor web"),
    MesAniversario(mes: "ago", profissao: "Quality Assurance"),
    MesAniversario(mes: "set", profissao: "Cientista de dados"),
    MesAniversario(mes: "out", profissao: "Administrador de rede"),
    MesAniversario(
        mes: "nov", profissao: "Administrador de segurança de dados"),
    MesAniversario(mes: "dez", profissao: "Analista de Business Intelligence"),
  ];

  String? _profissao;

  String? get profissao => _profissao;

  set profissao(String? profissao) {
    _profissao = profissao;
    notifyListeners();
  }
}
